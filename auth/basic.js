const User = require('../models/User');

module.exports = () => {
	return async (req, res, next) => {
		if (!req.authorization || !req.authorization.basic)
			return res.status(401).end();

		const {username, password} = req.authorization.basic;
		const user = await User.findOne().byUsername(username);
		if (!user) return res.status(401).end();

		const isAuthorized = await user.authorizeAsync(password);
		if (!isAuthorized) return res.status(401).end();

		req.userId = user._id.toString();
		if (req.body) req.body.user = req.userId;
		next();
	};
};
