const {JWT_SECRET} = process.env;

const jsonwebtoken = require('jsonwebtoken');
const util = require('util');
const jwtVerify = util.promisify(jsonwebtoken.verify);

module.exports = () => {
	return async (req, res, next) => {
		try {
			const token = req.authorization?.token
				?? req.cookies.token;

			if (!token)
				return res.status(401).end();

			const payload = await jwtVerify(token, JWT_SECRET);
			req.userId = payload.userId;
			if (req.body) req.body.user = payload.userId;
			next();
		}
		catch {
			res.status(401).end();
		}
	};
};
