require('dotenv').config();
const {COOKIE_SECRET, PORT, MONGODB, ORIGIN_URL} = process.env;

const mongoose = require('mongoose');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');
const express = require('express');
const {Users, Tasks} = require('./routers');
const {authParser} = require('./middleware');
const csrf = require('csurf');

mongoose.connect(MONGODB, {useNewUrlParser: true, useUnifiedTopology: true});

const app = express();
app.use(helmet());
app.use(cookieParser(COOKIE_SECRET));
app.use(csrf({cookie: true}));
app.use(express.urlencoded({extended: false}));
app.use(express.json())
app.use(authParser());
app.use(cors({
	origin: ORIGIN_URL,
	credentials: true,
	exposedHeaders: ['set-cookie']
}));
app.use('/api/users', Users());
app.use('/api/tasks', Tasks());

app.use('/api/csrf-token', (req, res) => {
	res.send({csrf: req.csrfToken()});
});

app.listen(PORT, () => console.log(`Task Button API listening on port ${PORT}`));
