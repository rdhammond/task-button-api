const Router = require('express').Router;
const restifyMongoose = require('restify-mongoose');
const {jwt} = require('../auth');
const {Task} = require('../models');

function filter(req, res) {
	return {user: req.userId};
}

module.exports = () => {
	const tasks = restifyMongoose(Task, {filter});

	const router = Router();
	router.get('/', jwt(), tasks.query());

	router.get('/random', jwt(), async (req, res) => {
		const query = {
			user: req.userId,
			$or: [
				{lastCompletedAt: {$exists: false}},
				{nextRefresh: {$lte: new Date()}}
			]
		};

		const count = await Task.countDocuments(query);
		if (count < 1) return res.send({});

		const index = Math.floor(Math.random() * count);
		const task = await Task.findOne(query)
			.skip(index)
			.limit(1)
			.lean();

		res.send(task ?? {});
	});

	router.get('/:id', jwt(), tasks.detail());
	router.post('/', jwt(), tasks.insert());
	router.patch('/:id', jwt(), tasks.update());
	router.delete('/:id', jwt(), tasks.remove());

	return router;
};
