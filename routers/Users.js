const {JWT_SECRET} = process.env;

const Router = require('express').Router;
const {User} = require('../models');
const restifyMongoose = require('restify-mongoose');
const {jwt, basic} = require('../auth');
const jsonwebtoken = require('jsonwebtoken');
const util = require('util');
const jwtSign = util.promisify(jsonwebtoken.sign);
const moment = require('moment');

const EXPIRES_IN_DAYS = '30d';

function filter(req, res) {
	return {_id: req.userId};
}

module.exports = () => {
	const tasks = restifyMongoose(User, {filter});

	const router = Router();
	router.get('/', jwt(), tasks.query());
	router.get('/me', jwt(), async (req, res, next) => {
		const user = await User.findById(req.userId).lean();
		res.send(user);
	});
	router.get('/:id', jwt(), tasks.detail());
	router.patch('/:id', jwt(), tasks.update());
	router.delete('/:id', jwt(), tasks.remove());

	// **TODO: Email Confirmation
	router.post('/', tasks.insert());

	router.post('/signin', basic(), async (req, res, next) => {
		const token = await jwtSign(
			{userId: req.userId},
			JWT_SECRET,
			{expiresIn: EXPIRES_IN_DAYS}
		)
		const user = await User.findById(req.userId).lean();

		// ** TODO: https only
		res.cookie('token', token, {
			maxAge: 1000 * 60 * 60 * 24 * parseInt(EXPIRES_IN_DAYS),
			httpOnly: true,
			sameSite: 'Strict'
		});
		res.send(user);
	});

	router.post('/signout', jwt(), async (req, res, next) => {
		res.clearCookie('token');
		res.status(204).end();
	});

	return router;
};
