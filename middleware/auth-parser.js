module.exports = () => {
	return (req, res, next) => {
		req.authorization = {};

		const authorization = req.header('authorization');
		if (!authorization) return next();

		const authInfo = authorization.split(' ', 2);
		if (authInfo.length < 2) return next();

		const [type, raw] = authInfo;
		req.authorization = {type, raw};

		switch (type) {
			case 'Basic':
				const decoded = Buffer.from(raw, 'base64').toString('utf-8').split(':', 2);
				if (decoded.length < 2) return next();
				req.authorization.basic = {username: decoded[0], password: decoded[1]};
				break;
			case 'Bearer':
				req.authorization.token = raw;
				break;
		}
		next();
	};
};
