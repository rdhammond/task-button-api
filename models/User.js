const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcrypt');
const _ = require('lodash');

const HASH_ROUNDS = 12;

function transform(doc, ret, options) {
	delete ret.password;
	return ret;
}

const userSchema = new mongoose.Schema({
	email: {type: String, required: true, validate: validator.isEmail, unique: true},
	password: {type: String, required: true},
	createdAt: {type: Date, required: true, default: Date.now},
	updatedAt: {type: Date, required: true, default: Date.now}
});
userSchema.set('toJSON', {transform});
userSchema.set('toObject', {transform});

userSchema.pre('save', async function(next) {
	if (!this.isModified())
		return next();
	
	if (this.isModified('password'))
		this.password = await bcrypt.hash(this.password, HASH_ROUNDS);

	this.updatedAt = new Date();
	next();
});

userSchema.query.byUsername = function(username) {
	return this.where({email: username});
};

userSchema.methods.authorizeAsync = function(password) {
	return bcrypt.compare(password, this.password);
};

module.exports = mongoose.model('User', userSchema);
