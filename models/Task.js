const moment = require('moment');
const mongoose = require('mongoose');
const _ = require('lodash');

const REPEAT = /^[0-9]+ (days?)|(weeks?)|(months?)|(years?)$/;

const taskSchema = new mongoose.Schema({
	user: {type: mongoose.Schema.ObjectId, required: true, ref: 'User'},
	text: {type: String, required: true, maxlength: 30},
	lastCompletedAt: Date,
	repeat: {type: String, match: REPEAT},
	nextRefresh: Date,
	createdAt: {type: Date, required: true, default: Date.now},
	updatedAt: {type: Date, required: true, default: Date.now}
});
taskSchema.index({user: 1, lastCompleted: 1, nextRefresh: 1});

taskSchema.pre('save', function(next) {
	if (!this.isModified())
		return next();

	const {lastCompletedAt, repeat} = this;

	if (this.isModified('lastCompletedAt repeat')) {
		if (lastCompletedAt && repeat) {
			const [amount, unit] = repeat.split(' ', 2);
			this.nextRefresh = moment(lastCompletedAt).add(amount, unit).toDate();
		}
		else {
			this.nextRefresh = undefined;
		}

		this.lastCompletedAt = this.lastCompletedAt || undefined;
		this.repeat = this.repeat || undefined;
	}

	this.updatedAt = new Date();
	next();
});

module.exports = mongoose.model('Task', taskSchema);
